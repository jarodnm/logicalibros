package navarro.jarod.logica.tl;
import navarro.jarod.logica.bl.*;
import navarro.jarod.logica.dl.*;

public class Controller {
    CL capa = new CL();

    public void registrarAutores(String nombre, String nacionalidad, String pseudonimo){
        Autor tmpautor = new Autor(nombre,nacionalidad,pseudonimo);
        capa.registrarAutores(tmpautor);
    }

    public void registrarNovela(String isbn, String nombre, int annioP, String nombreAutor, double precio){
        Libro tmplibro = new Libro(isbn,nombre,annioP,nombreAutor,precio);
        capa.registrarLibro(tmplibro);
    }

    public void registrarCuento(String isbn, String nombre, int annioP, String nombreAutor, double precio, String rangoEdad){
        Cuento tmpcuento = new Cuento(isbn,nombre,annioP,nombreAutor,precio,rangoEdad);
        capa.registrarLibro(tmpcuento);
    }

    public boolean autorExiste(String nombre){
        Autor tmpautor = new Autor();
        tmpautor.setNombre(nombre);
        return capa.autorExiste(tmpautor);
    }

    public boolean libroExiste(String isbn){
        Libro tmplibro = new Libro();
        tmplibro.setIsbn(isbn);
        return capa.libroExiste(tmplibro);
    }

    public String[] listarAutores(){
        return capa.listarAutores();
    }

    public String[] listarLibros(){
        return capa.listarLibros();
    }
}
