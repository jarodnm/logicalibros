package navarro.jarod.logica.bl;


import java.util.Objects;

public class Libro {
    private String isbn;
    private String nombre;
    private int annioP;
    private String nombreAutor;
    private double precio;

    public Libro() {
    }

    public Libro(String isbn, String nombre, int annioP, String nombreAutor, double precio) {
        this.isbn = isbn;
        this.nombre = nombre;
        this.annioP = annioP;
        this.nombreAutor = nombreAutor;
        this.precio = precio;
    }

    public String getIsbn() {
        return isbn;
    }

    public void setIsbn(String isbn) {
        this.isbn = isbn;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getAnnioP() {
        return annioP;
    }

    public void setAnnioP(int annioP) {
        this.annioP = annioP;
    }

    public String getNombreAutor() {
        return nombreAutor;
    }

    public void setNombreAutor(String nombreAutor) {
        this.nombreAutor = nombreAutor;
    }

    public double getPrecio() {
        return precio;
    }

    public void setPrecio(double precio) {
        this.precio = precio;
    }

    @Override
    public String toString() {
        return
                "ISBN: " + isbn +
                        ", nombre: " + nombre +
                        ", annioP: " + annioP +
                        ", nombreAutor: " + nombreAutor +
                        ", precio: " + precio + " Colones";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Libro libro = (Libro) o;
        return Objects.equals(isbn, libro.isbn);
    }
}
