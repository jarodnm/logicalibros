package navarro.jarod.logica.bl;

import java.util.Objects;

public class Autor {
    private String nombre;
    private String nacionalidad;
    private String pseudonimo;

    public Autor() {
    }

    public Autor(String nombre, String nacionalidad, String pseudonimo) {
        this.nombre = nombre;
        this.nacionalidad = nacionalidad;
        this.pseudonimo = pseudonimo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getNacionalidad() {
        return nacionalidad;
    }

    public void setNacionalidad(String nacionalidad) {
        this.nacionalidad = nacionalidad;
    }

    public String getPseudonimo() {
        return pseudonimo;
    }

    public void setPseudonimo(String pseudonimo) {
        this.pseudonimo = pseudonimo;
    }

    @Override
    public String toString() {
        return
                "Nombre: " + nombre +
                        ", nacionalidad: " + nacionalidad +
                        ", pseudonimo: " + pseudonimo;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Autor autor = (Autor) o;
        return Objects.equals(nombre, autor.nombre);
    }

}
