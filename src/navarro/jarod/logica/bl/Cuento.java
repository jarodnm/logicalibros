package navarro.jarod.logica.bl;

public class Cuento extends Libro {
    private String rangoEdad;

    public Cuento() {
        super();
    }

    public Cuento(String isbn, String nombre, int annioP, String nombreAutor, double precio, String rangoEdad) {
        super(isbn, nombre, annioP, nombreAutor, precio);
        this.rangoEdad = rangoEdad;
    }

    public String getRangoEdad() {
        return rangoEdad;
    }

    public void setRangoEdad(String rangoEdad) {
        this.rangoEdad = rangoEdad;
    }

    @Override
    public String toString() {
        return
                super.toString()+", rango de edad: " + rangoEdad ;

    }
}
