package navarro.jarod.logica.dl;

import java.util.*;

import navarro.jarod.logica.bl.*;

public class CL {

    static ArrayList<Autor> autores = new ArrayList<>();
    static ArrayList<Libro> libros = new ArrayList<>();

    public void registrarAutores(Autor tmpautor) {
        autores.add(tmpautor);
    }

    public void registrarLibro(Libro tmplibro) {
        libros.add(tmplibro);
    }

    public boolean autorExiste(Autor tmpAutor) {
        boolean existe = false;
        for (Autor dato : autores) {
            if (dato.equals(tmpAutor)) {
                existe = true;
            }
        }
        return existe;
    }

    public boolean libroExiste(Libro tmplibro) {
        boolean existe = false;
        for (Libro dato : libros) {
            if (dato.equals(tmplibro)) {
            existe = true;
            }
        }
        return existe;
    }

    public String[] listarLibros(){
        String datos[] = new String[libros.size()];
        int pos = 0;
        for(Libro dato : libros){
            datos[pos] = dato.toString();
            pos++;
        }
        return datos;
    }

    public String[] listarAutores(){
        String datos[] = new String[autores.size()];
        int pos = 0;
        for(Autor dato: autores){
            datos[pos] = dato.toString();
            pos++;
        }
        return datos;
    }

}

